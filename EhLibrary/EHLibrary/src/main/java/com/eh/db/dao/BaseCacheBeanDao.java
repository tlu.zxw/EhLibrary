package com.eh.db.dao;

import com.eh.cache.BaseCacheBean;
import com.eh.db.orm.dao.DbModel;

public class BaseCacheBeanDao extends DbModel<BaseCacheBean> {
    public BaseCacheBeanDao() {
        super(BaseCacheBean.class);
    }

}
