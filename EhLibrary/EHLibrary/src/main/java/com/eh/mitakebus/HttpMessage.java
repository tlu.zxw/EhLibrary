package com.eh.mitakebus;

/**
 * Created by zhangxiaowei on 17/4/25.
 */

public class HttpMessage extends BaseMessage {
    /**
     * 网络请求成功
     */
    public boolean success;
    /**
     * 对返回结果的描述
     */
    public String description;
    public String code;

    @Override
    public String toString() {
        return "HttpMessage{" +
                "success=" + success +
                ", description='" + description + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
