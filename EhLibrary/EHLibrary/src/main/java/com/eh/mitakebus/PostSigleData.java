package com.eh.mitakebus;

/**
 * Created by zhangxiaowei on 17/4/26.
 */

 class PostSigleData {
     public Object data;
    Class<?> sigleClazz;

    public PostSigleData(Object data, Class<?> sigleClazz) {
        this.data = data;
        this.sigleClazz = sigleClazz;
    }
}
