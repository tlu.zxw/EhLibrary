package com.eh.mitakebus;

import java.lang.reflect.Method;

/**
 * Created by zhangxiaowei on 17/4/28.
 */

final class SubscriberMethod {
     public Method method;
     public Class<?> eventType;
    /** Used for efficient comparison */

    SubscriberMethod(Method method, Class<?> eventType) {
        this.method = method;
        this.eventType = eventType;
    }


    @Override
    public int hashCode() {
        return method.hashCode();
    }
}
