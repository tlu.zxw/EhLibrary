package com.eh.cache;

import android.content.Context;

import com.eh.db.dao.BaseCacheBeanDao;

/**
 * 统一管理所有缓存信息，包括xml文件存储的密码
 * Created by zhangxiaowei on 16/4/6.
 */
public class CacheManager {
    private static Context mContext;
    private static CacheManager mCacheManager;
    private static byte[] byt = new byte[0];

    public static CacheManager getInstance(Context context) {
        mContext = context;
        if (mCacheManager == null) {
            synchronized (byt) {
                if (mCacheManager == null) {
                    mCacheManager = new CacheManager();
                }
            }
        }
        return mCacheManager;
    }


    /**
     * 获取缓存的url信息
     */
    public BaseCacheBean getCacheInfo(String id) {
        BaseCacheBeanDao dao = new BaseCacheBeanDao();
        BaseCacheBean bean = dao.queryOne(id);
        return bean;
    }

    /**
     * 删除缓存的信息
     */
    public long removeCacheInfo(String id) {
        BaseCacheBeanDao dao = new BaseCacheBeanDao();
        return dao.delete(id);
    }

    /**
     * 存入缓存的信息
     */
    public void putCacheInfo(BaseCacheBean cacheBean) {
        BaseCacheBeanDao dao = new BaseCacheBeanDao();
        BaseCacheBean bean = dao.queryOne(cacheBean.getId());
        if (bean == null) {
            dao.insert(cacheBean);
        } else {
            dao.update(cacheBean);
        }
    }

    /**
     * 清除所有http请求的缓存
     */
    public void clearAllHttpCache() {
        BaseCacheBeanDao dao = new BaseCacheBeanDao();
        dao.deleteAll();
    }

    /**
     * 清除所有http请求的缓存
     */
    public void logOut() {
    }


}
