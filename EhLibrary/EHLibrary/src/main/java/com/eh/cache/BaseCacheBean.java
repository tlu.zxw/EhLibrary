package com.eh.cache;

import com.eh.db.orm.annotation.Column;
import com.eh.db.orm.annotation.Id;
import com.eh.db.orm.annotation.Table;

import java.io.Serializable;

/**
 * Created by zhangxiaowei on 16/4/5.
 */
@Table(name = "BaseCacheBean")
public class BaseCacheBean implements Serializable {
    /**
     * 缓存的url作为id
     */
    @Id
    @Column(name = "id")
    private String id;

    /**
     * 用于标明该条缓存使用的条件，比如不同用户都是用医院信息，可指定hospitalid为该id
     */
    @Column(name = "cacheId")
    private String cacheId;


    /**
     * 对应URL返回的json数据
     */
    @Column(name = "resultJson")
    private String resultJson;
    /**
     * 缓存持续的时长
     */
    @Column(name = "durationTime")
    private long durationTime;
    /**
     * 缓存存入的时间
     */
    @Column(name = "inputTime")
    private long inputTime;
    /**
     * 描述
     */
    @Column(name = "upLoadData")
    private String upLoadData;
    /**
     * 是否使用缓存与HTTP请求同时进行
     */
    @Column(name = "isCacheAndHttp")
    private boolean isCacheAndHttp;
    /**
     * 请求的类型
     */
    @Column(name = "apiHttp")
    private int apiHttp;

    @Override
    public String toString() {
        return "BaseCacheBean{" +
                "id='" + id + '\'' +
                ", cacheId='" + cacheId + '\'' +
                ", resultJson='" + resultJson + '\'' +
                ", durationTime=" + durationTime +
                ", inputTime=" + inputTime +
                ", upLoadData='" + upLoadData + '\'' +
                ", isCacheAndHttp=" + isCacheAndHttp +
                ", apiHttp=" + apiHttp +
                '}';
    }

    public int getApiHttp() {
        return apiHttp;
    }


    public void setApiHttp(int apiHttp) {
        this.apiHttp = apiHttp;
    }

    public String getCacheId() {
        return cacheId;
    }

    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }

    public String getResultJson() {
        return resultJson;
    }

    public void setResultJson(String resultJson) {
        this.resultJson = resultJson;
    }

    public long getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(long durationTime) {

        this.durationTime = durationTime * 1000;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getInputTime() {
        return inputTime;
    }

    public void setInputTime(long inputTime) {
        this.inputTime = inputTime;
    }

    public String getUpLoadData() {
        return upLoadData;
    }

    public void setUpLoadData(String upLoadData) {
        this.upLoadData = upLoadData;
    }


    public boolean isCacheAndHttp() {
        return isCacheAndHttp;
    }

    public void setCacheAndHttp(boolean cacheAndHttp) {
        isCacheAndHttp = cacheAndHttp;
    }
}
