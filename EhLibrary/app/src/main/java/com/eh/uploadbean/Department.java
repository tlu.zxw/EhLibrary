package com.eh.uploadbean;

import com.eh.db.orm.annotation.Column;
import com.eh.db.orm.annotation.Table;

@Table(name = "Department")
public class Department {

    @Column(name = "id")
    int id;
    @Column(name = "s_id")
    int s_id;
    @Column(name = "name")
    String name;
    @Column(name = "stu_num")
    String stu_num;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStu_num() {
        return stu_num;
    }

    public void setStu_num(String stu_num) {
        this.stu_num = stu_num;
    }

    @Override
    public String toString() {
        return "Department [id=" + id + ", s_id=" + s_id + ", name=" + name + ", stu_num=" + stu_num + "]";
    }

}
