package com.eh.uploadbean;

import com.eh.db.orm.annotation.Column;
import com.eh.db.orm.annotation.RelationDao;
import com.eh.db.orm.annotation.RelationsType;
import com.eh.db.orm.annotation.Table;

import java.util.List;

@Table(name = "School")
public class School {

    @Column(name = "s_id")
    int s_id;
    @Column(name = "name")
    String name;
    @Column(name = "type")
    String type;
    @Column(name = "stunum")
    String stunum;
    @Column(name = "stuid")
    String stuid;
    @Column(name = "age")
    String age;


    @RelationDao(name = "s_id", foreignKey = "s_id", type = RelationsType.one2one)
    @Column(name = "Department")
    Department department;

    @RelationDao(name = "s_id", foreignKey = "s_id", type = RelationsType.one2many)
    @Column(name = "DepartmentInfo")
    List<DepartmentInfo> departmentinfo;

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getName() {
        return name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStunum() {
        return stunum;
    }

    public void setStunum(String stunum) {
        this.stunum = stunum;
    }

    public String getStuid() {
        return stuid;
    }

    public void setStuid(String stuid) {
        this.stuid = stuid;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "School{" +
                "s_id=" + s_id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", stunum='" + stunum + '\'' +
                ", stuid='" + stuid + '\'' +
                ", age='" + age + '\'' +
                ", department=" + department +
                ", departmentinfo=" + departmentinfo +
                '}';
    }

    public List<DepartmentInfo> getDepartmentinfo() {
        return departmentinfo;
    }

    public void setDepartmentinfo(List<DepartmentInfo> departmentinfo) {
        this.departmentinfo = departmentinfo;
    }

}
