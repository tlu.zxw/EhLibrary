package com.eh.uploadbean;

import com.eh.db.orm.annotation.Column;

import java.io.Serializable;

/**
 * Created by zhangxiaowei on 16/3/16.
 */
public class BaseUploadBean implements Serializable {
    /**
     * 时间戳
     */
    @Column(name = "client_time")
    public String client_time;

    public String getClient_time() {
        return client_time;
    }

    public void setClient_time(String client_time) {
        this.client_time = client_time;
    }
}
