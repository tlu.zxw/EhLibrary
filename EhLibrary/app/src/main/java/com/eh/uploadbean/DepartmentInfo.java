package com.eh.uploadbean;

import com.eh.db.orm.annotation.Column;
import com.eh.db.orm.annotation.Table;

import java.io.Serializable;

@Table(name = "DepartmentInfo")
public class DepartmentInfo implements Serializable {
    @Column(name = "coach_id")
    private int coach_id;
    @Column(name = "s_id")
    private int s_id;
    @Column(name = "peonum")
    private String peonum;

    public int getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(int coach_id) {
        this.coach_id = coach_id;
    }

    public int getS_id() {
        return s_id;
    }

    @Override
    public String toString() {
        return "DepartmentInfo [coach_id=" + coach_id + ", s_id=" + s_id + ", peonum=" + peonum + "]";
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getPeonum() {
        return peonum;
    }

    public void setPeonum(String peonum) {
        this.peonum = peonum;
    }

}
