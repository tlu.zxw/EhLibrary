package com.eh.uploadbean;

import com.eh.db.orm.annotation.Column;
import com.eh.db.orm.annotation.Id;
import com.eh.db.orm.annotation.RelationDao;
import com.eh.db.orm.annotation.RelationsType;
import com.eh.db.orm.annotation.Table;

/**
 * Created by zhangxiaowei on 16/11/10.
 */
@Table(name = "LoginBean")
public class LoginBean {

    public LoginBean() {
    }

    @Id
    @Column(name = "userid")
    int userid;

    @Column(name = "us_id")
    int us_id;


    @RelationDao(name = "us_id", foreignKey = "s_id", type = RelationsType.one2one)
    @Column(name = "School")
    School school;

    @Id
    @Column(name = "account")
    String account;
    @Column(name = "userName")
    String userName;
    @Column(name = "password")
    String password;


    @Column(name = "name")
    String name;
    @Column(name = "phone")
    String phone;
    @Column(name = "age")
    String age;

    @Column(name = "sex")
    String sex;
    @Column(name = "photo_url")
    String photo_url;


    @Override
    public String toString() {
        return "LoginBean{" +
                "us_id=" + us_id +
                ", school=" + school +
                ", account='" + account + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", userid=" + userid +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", age='" + age + '\'' +
                ", sex='" + sex + '\'' +
                ", photo_url='" + photo_url + '\'' +
                '}';
    }

    public LoginBean(String name, String pwd) {
        this.userName = name;
        this.password = pwd;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
