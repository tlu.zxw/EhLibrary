package com.eh.act;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.eh.bean.BaseBean;
import com.eh.eventbus.EventBus;
import com.eh.library.R;
import com.eh.mitakebus.HttpMessage;
import com.eh.mitakebus.MitakeBus;
import com.eh.utils.LogUtil;

/**
 * Created by zhangxiaowei on 17/4/26.
 */

public class SecondAct extends Activity {
    private final String TAG =SecondAct.class.getSimpleName();

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_second);
        LogUtil.i(true,TAG, "SecondAct: onCreate: [savedInstanceState]=");
        MitakeBus.getDefaut().register(this);
        LogUtil.i(true,TAG, "SecondAct: onCreate: [0000000]=");
        button= (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.i("SecondAct", "SecondAct:onClick: [v]=2222");
//                MitakeBus.getDefaut().post("5555555");
//                MitakeBus.getDefaut().post("555555500", MainAct.class);
                MitakeBus.getDefaut().post("66666", SecondAct.class);
                LogUtil.i("SecondAct", "SecondAct:onClick: [v333]=");
            }
        });
    }

    public void onMitakeData(String string) {
        try {
            button.setText(string);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, string + "++++", Toast.LENGTH_SHORT).show();
    }
    public void onEventMainThread(String string) {
        try {
            button.setText(string);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, string + "++++", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        MitakeBus.getDefaut().unRegister(this);
    }

}
