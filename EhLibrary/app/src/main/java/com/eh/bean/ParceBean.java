package com.eh.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhangxiaowei on 17/4/19.
 */

public interface ParceBean<T> extends Parcelable {

    T createFromParce(Parcel in);
    T[]  newArray(int size);

    Creator1 CREATOR_1=new Creator1() {
        @Override
        public Object createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public Object[] newArray(int size) {
            return new Object[0];
        }
    };
    public interface Creator1 extends Creator{

    }

}
