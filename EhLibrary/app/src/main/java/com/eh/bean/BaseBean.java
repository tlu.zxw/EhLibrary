package com.eh.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.ParameterizedType;

/**
 * Created by zhangxiaowei on 17/4/18.
 */

public abstract class BaseBean<T extends BaseBean> implements Parcelable {

    public BaseBean() {
    }

    public BaseBean(Parcel in) {

    }

    public abstract T createFromParcel(Parcel in);
    public abstract T[]  newArray(int size);

    public static   final Creator<BaseBean> CREATOR
            = new Creator<BaseBean>() {
        public BaseBean createFromParcel(Parcel in) {
            return createFromParcel(in);
        }

        public BaseBean[] newArray(int size) {
            return newArray(size);
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(id);
//        dest.writeString(corp);
//        dest.writeString(corporation);
//        dest.writeInt(state);

    }

}
