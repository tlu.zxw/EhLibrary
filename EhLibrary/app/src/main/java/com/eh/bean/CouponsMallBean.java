package com.eh.bean;

import java.io.Serializable;

/**
 * Created by zhangxiaowei on 16/11/22.
 */

public class CouponsMallBean implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
