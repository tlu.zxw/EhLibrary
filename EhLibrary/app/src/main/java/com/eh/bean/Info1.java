package com.eh.bean;

/**
 * Created by zhangxiaowei on 17/3/20.
 */

public class Info1 {
    String name;

    @Override
    public String toString() {
        return "Info1{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
