package com.eh.bean;

import java.util.List;

/**
 * Created by zhangxiaowei on 17/3/3.
 */

public class JsonBean {
    List<JsonContent> programmers;
    JsonContent programmerlls;
    @Override
    public String toString() {
        return "JsonBean{" +
                "programmers=" + programmers + "programmerlls=" + programmerlls +
                '}';
    }

    public List<JsonContent> getProgrammers() {
        return programmers;
    }

    public void setProgrammers(List<JsonContent> programmers) {
        this.programmers = programmers;
    }
}
