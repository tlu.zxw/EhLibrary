package com.eh.bean;

import com.eh.db.orm.annotation.Column;
import com.eh.db.orm.annotation.Table;

/**
 * Created by zhangxiaowei on 17/5/24.
 */
@Table(name = "SearchResultItemBean")
public class SearchResultItemBean {
    @Column(name = "stockID")
    public String stockID;
    @Column(name = "name")
    public String name;
    @Column(name = "subtype")
    public String subtype;
    @Column(name = "pinyin")
    public String pinyin;
    @Column(name = "zimu")
    public String zimu;
    @Column(name = "thread")
    public String thread;
    @Column(name = "hktype")
    public String hktype;

    @Override
    public String toString() {
        return "SearchResultItemBean{" +
                "stockID='" + stockID + '\'' +
                ", name='" + name + '\'' +
                ", subtype='" + subtype + '\'' +
                ", pinyin='" + pinyin + '\'' +
                ", zimu='" + zimu + '\'' +
                ", thread='" + thread + '\'' +
                ", hktype='" + hktype + '\'' +
                '}';
    }

    public String getHktype() {
        return hktype;
    }

    public void setHktype(String hktype) {
        this.hktype = hktype;
    }

    public String getThread() {
        return thread;
    }

    public void setThread(String thread) {
        this.thread = thread;
    }

    public String getStockID() {
        return stockID;
    }

    public void setStockID(String stockID) {
        this.stockID = stockID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getZimu() {
        return zimu;
    }

    public void setZimu(String zimu) {
        this.zimu = zimu;
    }
}
