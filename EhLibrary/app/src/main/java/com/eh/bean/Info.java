package com.eh.bean;

import java.util.List;

/**
 * Created by zhangxiaowei on 17/3/20.
 */

public class Info {
    List<String> info1;

    @Override
    public String toString() {
        return "Info{" +
                "info1=" + info1 +
                '}';
    }

    public List<String> getInfo1() {
        return info1;
    }

    public void setInfo1(List<String> info1) {
        this.info1 = info1;
    }
}
