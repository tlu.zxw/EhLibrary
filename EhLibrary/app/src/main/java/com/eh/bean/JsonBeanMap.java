package com.eh.bean;

import java.util.List;
import java.util.Map;

/**
 * Created by zhangxiaowei on 17/3/3.
 */

public class JsonBeanMap {
    List<Map<String,String>> programmers;
    JsonContent programmerlls;
    @Override
    public String toString() {
        return "JsonBean{" +
                "programmers=" + programmers + "programmerlls=" + programmerlls +
                '}';
    }

    public  List<Map<String,String>> getProgrammers() {
        return programmers;
    }

    public void setProgrammers( List<Map<String,String>> programmers) {
        this.programmers = programmers;
    }
}
