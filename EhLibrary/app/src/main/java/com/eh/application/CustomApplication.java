package com.eh.application;

import android.app.Application;
import android.os.Environment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.eh.bean.SearchResultItem;
import com.eh.bean.SearchResultItemBean;
import com.eh.cache.BaseCacheBean;
import com.eh.db.dao.DbFactory;
import com.eh.logutil.LogUtil;
import com.eh.uploadbean.Department;
import com.eh.uploadbean.DepartmentInfo;
import com.eh.uploadbean.LoginBean;
import com.eh.uploadbean.School;
import com.eh.utils.CrashHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
//import com.squareup.leakcanary.LeakCanary;


public class CustomApplication extends Application {
    private final String TAG = LogUtil.DEGUG_MODE ? "CustomApplication"
            : CustomApplication.class.getSimpleName();
    public final String DIR = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/eh/log/";
    public final String NAME = getCurrentDateString() + ".txt";

    /**
     * 字体
     */

    public static RequestQueue mVolleyQueue;

    static CustomApplication mContextApplication;
    // 数据库名
    private static  String DBNAME = "ehpos.db";
    // 当前数据库的版本
    private static  int DBVERSION = 25;
    // 要初始化的表
    private static  Class<?>[] mClazz = {
            BaseCacheBean.class,
            LoginBean.class,
            School.class,
            Department.class,
            DepartmentInfo.class,
            SearchResultItem.class,
            SearchResultItemBean.class
    };


    /**
     * 捕获错误信息的handler
     */
    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            String info = null;
            ByteArrayOutputStream baos = null;
            PrintStream printStream = null;
            try {
                baos = new ByteArrayOutputStream();
                printStream = new PrintStream(baos);
                ex.printStackTrace(printStream);
                byte[] data = baos.toByteArray();
                info = new String(data);
            } catch (Exception e) {
                e.printStackTrace();
                LogUtil.i(true, TAG, "CustomApplication: eeeeeeee"
                        + e);
            } finally {
                try {
                    if (printStream != null) {
                        printStream.close();
                    }
                    if (baos != null) {
                        baos.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            LogUtil.i(true, TAG, "CustomApplication: eeeeeeee"
                    + info);

        }
    };

    /**
     * 获取当前日期
     *
     * @return
     */
    private static String getCurrentDateString() {
        String result = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
                Locale.getDefault());
        Date nowDate = new Date();
        result = sdf.format(nowDate);
        return result;
    }

    /**
     * 向文件中写入错误信息
     *
     * @param info
     */
    protected void writeErrorLog(String info) {
        File dir = new File(DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir, NAME);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            fileOutputStream.write(info.getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static CustomApplication getInstance() {
        return mContextApplication;

    }


    @Override
    public void onCreate() {
        super.onCreate();

        mContextApplication = this;
//        LeakCanary.install(this);
//        ImageloadUtil.initImageCacheFramework();// 初始化图片缓存框架
        CrashHandler.getInstance().init(this);
        mVolleyQueue = Volley.newNoCacheRequestQueue(getApplicationContext());
        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
        DbFactory.init(getApplicationContext(),mClazz,DBNAME,DBVERSION);
    }


}
