package com.eh.adapter;

import android.content.Context;
import android.widget.TextView;

import com.eh.bean.CouponsMallBean;
import com.eh.bean.SearchResultItem;
import com.eh.library.R;

import java.util.List;

/**
 * Created by zhangxiaowei on 16/11/21.
 */

public class TestAdapter extends BaseRecyclerAdp<SearchResultItem, TestAdapter.MallAdapter> {

    public TestAdapter(Context context, List<SearchResultItem> beans) {
        super(context, beans);
    }

    @Override
    public void onCreateItemView() {
        addItemView(R.layout.item_test);
    }

    @Override
    public void onBindViewHolder(MallAdapter holder, SearchResultItem data, int position, int viewType) {
        holder.name.setText(data.toString()+"  ppp=  "+position);
    }

    @Override
    public MallAdapter getChildHolder(BaseAdapterEh holder, int viewType) {
        return new MallAdapter(holder, viewType);
    }

    public class MallAdapter extends BaseRecyclerViewHolder {
        TextView name;

        public MallAdapter(BaseAdapterEh<MallAdapter> holder, int viewType) {
            super(holder, viewType);
            name = holder.findView(R.id.name);
        }
    }
}
