package com.contacts;

/**
 * Copyright
 *
 * @author zhangxiaowei
 */
public interface ApiMessage {
     int API_LOGIN = 1;
    /**
     * 扫码支付
     */
     int API_EPAY_CODE = 2;
    /**
     * NFC支付
     */
     int API_EPAY_NFC = 3;

    /**
     * yandan
     */
     int API_YAN_DAN = 4;

    /**
     * yandan完成
     */
     int API_YAN_DAN_FINISH = 5;
    /**
     * 查询卡信息
     */
     int API_CARD_INFO = 6;
    /**
     * 打卡
     */
     int API_DA_KA = 7;
    /**
     *打印
     */
     int API_PRINT_DATA = 8;

    /**
     * 我要打印
     */
     int API_WOYAO_PRINT = 9;
    /**
     * 食堂取消订单
     */
     int API_CANCEL_CANTEEN_ORDER = 10;
    /**
     * 离线打卡上传数据
     */
     int API_PUNCH_OFFLINE_UPLOAD = 11;
    /**
     * 离线收款上传数据
     */
     int API_EPAY_OFFLINE_UPLOAD = 12;
    /**
     *  nfc写卡
     */
     int API_NFC_WRITE = 13;
    /**
     *  获取医院用户信息
     */
     int API_EMPLOYEE_INFO = 14;


}
