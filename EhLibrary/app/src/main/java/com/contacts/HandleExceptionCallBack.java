package com.contacts;

public interface HandleExceptionCallBack {
	public void callback(Throwable ex);
}