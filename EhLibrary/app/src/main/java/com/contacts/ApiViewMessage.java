package com.contacts;

/**
 * Copyright
 * 
 * @author zhangxiaowei
 * 
 */
public interface ApiViewMessage {
	 int ROOT = 1;
	/**
	 * 接收到消息，付款
	 */
	 int VIEW_JPUSH_NOTIFY = 101;
	/**
	 * 支付成功
	 */
	 int VIEW_PAY_SUCCESS = 102;
	/**
	 * 考勤成功
	 */
	 int VIEW_KAOQIN_SUCCESS = 103;

	/**
	 * 收款成功，只针对固额收款
	 */
	 int VIEW_PAY_SUCCESS_GU_E = 104;
	/**
	 * 普通收款，点击返回，关闭下一层NFc界面
	 */
	 int VIEW_BACK_RECEIPT = 105;
	/**
	 * 屏幕熄灭状态
	 */
	 int VIEW_SCREEN_OFF = 106;
	/**
	 * 后台运行状态
	 */
	 int VIEW_IN_BACKGROUND = 107;
	/**
	 * 后台运行状态
	 */
	 int VIEW_IN_FROEGROUND = 108;

	/**
	 * 楼层，区域
	 */
	 int VIEW_LOU_CENG = 109;

	 int VIEW_QU_YU = 110;

	/**
	 * 打印
	 */
	 int DA_YIN = 111;
	/**
	 * 打印完成
	 */
	 int DA_YIN_FINISH = 112;


	/**
	 * 打印完成
	 */
	 int CLICK_YIDAYIN = 113;


	/**
	 * 打印完成
	 */
	 int VIEW_PUNCH_OFFLINE_UPLOAD_SUCCESS = 114;




}
