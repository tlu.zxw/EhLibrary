package com.dbdao;

import com.eh.db.orm.dao.DbModel;
import com.eh.uploadbean.LoginBean;

public class LoginUpBeanDao extends DbModel<LoginBean> {
    public LoginUpBeanDao() {
        super(LoginBean.class);
    }
}
