//package com.http;
//
//import android.content.Context;
//
//import com.alibaba.fastjson.JSON;
//import com.easyhospital.pos.aes.AESUtil;
//import com.easyhospital.pos.cache.BaseCacheBean;
//import com.easyhospital.pos.message.HttpMessage;
//import com.easyhospital.pos.uploadbean.BaseUploadBean;
//import com.easyhospital.pos.utils.AbKeys;
//import com.eh.logutil.LogUtil;
//import com.eh.uploadbean.BaseUploadBean;
//
//import org.greenrobot.eventbus.EventBus;
//
//import java.util.HashMap;
//import java.util.Map;
//
//
///**
// * Created by zhangxiaowei on 16/4/5.
// */
//public class HttpRequest {
//    private static final String TAG = LogUtil.DEGUG_MODE ? "HttpRequest"
//            : HttpRequest.class.getSimpleName();
//
//    /**
//     * 不使用缓存的请求
//     */
//    public static void httpPostRequest(
//            Context context, RseponseHander hander
//    ) {
//
//        BaseUploadBean bean = (BaseUploadBean) hander.getDataBean();
//        bean.setClient_time(System.currentTimeMillis() + "");
//        String json = JSON.toJSONString(bean);
//        LogUtil.i(true, TAG, "HttpRequest: httpPostRequest: [222222]=" + json);
//
//        Map<String, String> pair = new HashMap<String, String>();
//        pair.put(AbKeys.DATA, AESUtil.encrypt(json));
//        try {
//            HttpVolley volley = new HttpVolley(context);
//            volley.post(
//                    pair,
//                    hander
//            );
//        } catch (Exception e) {
//            EventBus.getDefault().post(new HttpMessage(hander.mEent, false, e.toString(), e.toString(), "-01"));
//        }
//    }
//
//    /**
//     * 不使用缓存的请求
//     */
//    public static void httpPostRequest(
//            Context context, RseponseHander hander, BaseCacheBean cacheBean
//    ) {
//        BaseUploadBean bean = (BaseUploadBean) hander.getDataBean();
//        bean.setClient_time(System.currentTimeMillis() + "");
//        String json = JSON.toJSONString(bean);
//        LogUtil.i(true, TAG, "HttpRequest: httpPostRequest: [1111111]=" + json);
//        Map<String, String> pair = new HashMap<String, String>();
//        pair.put(AbKeys.DATA, AESUtil.encrypt(json));
//        try {
//            HttpVolley volley = new HttpVolley(context);
//            volley.post(
//                    pair,
//                    hander, cacheBean
//            );
//        } catch (Exception e) {
//            EventBus.getDefault().post(new HttpMessage(hander.mEent, false, e.toString(), e.toString(), "-01"));
//        }
//    }
//
//
//}
