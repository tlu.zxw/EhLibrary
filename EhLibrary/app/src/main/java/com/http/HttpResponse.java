package com.http;

import java.io.Serializable;
import java.util.Map;

/**
 * Copyright HTTP API接口请求返回的数据对象封装
 *
 * @author zhangxiaowei
 */
public class HttpResponse<T> implements Serializable {
    public T data = null; // 返回的数据
    public String msg_code = ""; // 描述信息
    public String msg = ""; // 结果码
    public String server_time;


    @Override
    public String toString() {
        return "HttpResponse{" +
                "data=" + data +
                ", msg_code='" + msg_code + '\'' +
                ", msg='" + msg + '\'' +
                ", server_time='" + server_time + '\'' +
                '}';
    }
}


