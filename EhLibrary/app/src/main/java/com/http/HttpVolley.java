//package com.http;
//
//import android.content.Context;
//import android.support.annotation.NonNull;
//
//import com.alibaba.fastjson.JSON;
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request.Method;
//import com.android.volley.Response.ErrorListener;
//import com.android.volley.Response.Listener;
//import com.android.volley.Response.LoadingListener;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.easyhospital.pos.application.CustomApplication;
//import com.easyhospital.pos.cache.BaseCacheBean;
//import com.easyhospital.pos.cache.CacheManager;
//import com.easyhospital.pos.eventbus.EventBus;
//import com.easyhospital.pos.message.HttpMessage;
//import com.easyhospital.pos.utils.AbKeys;
//import com.easyhospital.pos.utils.NetUtil;
//import com.easyhospital.pos.utils.ServiceType;
//import com.easyhospital.pos.utils.StringUtils;
//
//import java.util.Map;
//
//
//public class HttpVolley {
//    private final String TAG = LogUtil.DEGUG_MODE ? "HttpVolley"
//            : HttpVolley.class.getSimpleName();
//    Context mContext;
//    RseponseHander mResponseHander;
//    BaseCacheBean mCacheBean;
//
//    public HttpVolley(Context context) {
//        mContext = context;
//    }
//
//    public void get(
//            final Map<String, String> parms, RseponseHander respon) {
//        mResponseHander = respon;
//        if (!NetUtil.isConnect(mContext)) {
//            if (mResponseHander.mThirdPrtyListener == null) {
//                loadResult(null, false, "网络连接失败", "-0");
//            } else {
//                HttpMessage httpMessage = new HttpMessage(respon.mEent, false,
//                        "网络连接失败", null, "-0", respon.mObj);
//                mResponseHander.mThirdPrtyListener.onErrorResponse(httpMessage);
//            }
//            return;
//        }
//        getRequest(parms);
//
//    }
//
//    private void getRequest(final Map<String, String> parms) {
//        StringRequest request = new StringRequest(Method.GET, mResponseHander.getmUrl(),
//                new Listener<String>() {
//
//                    @Override
//                    public void onResponse(String response) {
//                        if (mResponseHander.mThirdPrtyListener == null) {
//                            handleResponseContent(response);
//                        } else {
//                            Object obj = null;
//                            if (mResponseHander.mResultAllToken != null) {
//                                obj = JSON.parseObject(response, mResponseHander.mResultAllToken.getType());
//                            }
//                            HttpMessage message = new HttpMessage(mResponseHander.mEent, true, "",
//                                    obj, "", mResponseHander.mObj);
//                            mResponseHander.mThirdPrtyListener.onSuccess(message);
//                            if (mCacheBean != null) {
//                                saveCache(response);
//                            }
//                        }
//
//                    }
//                }, new ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                String str = error.toString();
//                String desprition = null;
//                String errorcode;
//                if (str.contains("TimeoutError")) {
//                    desprition = "网络连接超时";
//                    errorcode = ServiceType.TIME_OUT;
//                } else if (str.contains("NetworkError")) {
//                    desprition = "网络连接失败";
//                    errorcode = ServiceType.NET_WORK_ERROR;
//                } else {
//                    desprition = "服务器连接失败";
//                    if (error.networkResponse == null) {
//                        errorcode = ServiceType.SERVICE_ERROR;
//                    } else {
//                        errorcode = error.networkResponse.statusCode + "";
//                    }
//                }
//
//                if (mResponseHander.mThirdPrtyListener == null) {
//                    loadResult(null, false, desprition, errorcode);
//                } else {
//                    HttpMessage httpMessage = new HttpMessage(mResponseHander.mEent, false,
//                            desprition, null, errorcode,
//                            mResponseHander.mObj);
//                    mResponseHander.mThirdPrtyListener.onErrorResponse(httpMessage);
//                }
//                System.gc();
//            }
//
//        }, new LoadingListener() {
//
//            @Override
//            public void onLoading(long count, long current) {
//
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                return parms;
//
//            }
//
//        };
//        request.setTag(mResponseHander.getmUrl());
//        CustomApplication.mVolleyQueue.add(request);
//    }
//
//
//    public void post(
//            final Map<String, String> parms,
//            RseponseHander responseHander) {
//        LogUtil.i(true, TAG, "【HttpVolley.post()】【url=" + responseHander.getmUrl() + ",parms="
//                + parms + "】");
//        mResponseHander = responseHander;
//        if (!NetUtil.isConnect(mContext)) {
//            if (mResponseHander.mThirdPrtyListener == null) {
//                loadResult(null, false, "网络连接失败", "-0");
//            } else {
//                HttpMessage httpMessage = new HttpMessage(mResponseHander.mEent, false,
//                        "网络连接失败", null, "-0", mResponseHander.mObj);
//                mResponseHander.mThirdPrtyListener.onErrorResponse(httpMessage);
//            }
//            return;
//        }
//
//        postRequest(parms);
//    }
//
//    /**
//     * 缓存请求
//     *
//     * @param parms
//     * @param responseHander
//     * @param cacheBean
//     */
//    public void post(
//            final Map<String, String> parms,
//            RseponseHander responseHander,
//            BaseCacheBean cacheBean) {
//        LogUtil.i(true, TAG, "【HttpVolley.post()】【url=" + responseHander.getmUrl() + ",parms="
//                + parms + "】");
//        mResponseHander = responseHander;
//        if (!NetUtil.isConnect(mContext)) {
//            if (mResponseHander.mThirdPrtyListener == null) {
//                loadResult(null, false, "网络连接失败", "-0");
//            } else {
//                HttpMessage httpMessage = new HttpMessage(mResponseHander.mEent, false,
//                        "网络连接失败", null, "-0", mResponseHander.mObj);
//                mResponseHander.mThirdPrtyListener.onErrorResponse(httpMessage);
//            }
//            return;
//        }
//        if (cacheBean == null) {
//            post(parms, responseHander);
//            return;
//        }
//        StringBuilder id = new StringBuilder(mResponseHander.getmUrl());
//        if (mResponseHander.mObj != null && mResponseHander.mObj.length > 0) {
//            id.append(mResponseHander.mObj[0]);
//        }
//        id.append(parms.get(AbKeys.DATA));
//        mCacheBean = cacheBean;
////        mCacheBean.setUpLoadData(parms.get(AbKeys.DATA));
//        mCacheBean.setId(id.toString());
//
//        //缓存与HTTP共存使用
//        if (mCacheBean.isCacheAndHttp()) {
//
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    BaseCacheBean bean = CacheManager.getInstance(mContext).getCacheInfo(mCacheBean.getId());
//                    if (bean != null && (bean.getCacheId() != null)
//                            && bean.getCacheId().equals(mCacheBean.getCacheId())) {
//                        if (mResponseHander.mThirdPrtyListener == null) {
//                            handleCacheContent(bean.getResultJson());
//                        } else {
//                            Object obj = null;
//                            if (mResponseHander.mResultAllToken != null) {
//                                obj = JSON.parseObject(bean.getResultJson(), mResponseHander.mResultAllToken.getType());
//                            }
//                            HttpMessage message = new HttpMessage(mResponseHander.mEent, true, "",
//                                    obj, "", mResponseHander.mObj);
//                            mResponseHander.mThirdPrtyListener.onSuccess(message);
//                        }
//                    }
//                    postRequest(parms);
//                }
//            }).start();
//
//        } else {//先取缓存,后访问网络
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    BaseCacheBean bean = CacheManager.getInstance(mContext).getCacheInfo(mCacheBean.getId());
//                    if (bean == null) {
//                        postRequest(parms);
//                    } else {
//                        long duration = mCacheBean.getDurationTime();
//                        long puttime = bean.getInputTime();
//                        long time = System.currentTimeMillis();
//                        if ((time - puttime) <= duration && (bean.getCacheId() != null)
//                                && bean.getCacheId().equals(mCacheBean.getCacheId())) {
//                            if (mResponseHander.mThirdPrtyListener == null) {
//                                handleCacheContent(bean.getResultJson());
//                            } else {
//                                Object obj = null;
//                                if (mResponseHander.mResultAllToken != null) {
//                                    obj = JSON.parseObject(bean.getResultJson(), mResponseHander.mResultAllToken.getType());
//                                }
//                                HttpMessage message = new HttpMessage(mResponseHander.mEent, true, "",
//                                        obj, "", mResponseHander.mObj);
//                                mResponseHander.mThirdPrtyListener.onSuccess(message);
//                            }
//                        } else {
//                            postRequest(parms);
//                            /**
//                             * 过期的缓存删除
//                             */
//                            long rrr = CacheManager.getInstance(mContext).removeCacheInfo(mCacheBean.getId());
//                            LogUtil.i(true, TAG, "HttpVolley: post: [000000000]=" + rrr);
//                        }
//                    }
//                }
//            }).start();
//
//        }
//
//    }
//
//    /**
//     * 缓存请求
//     *
//     * @param parms
//     * @param responseHander
//     * @param cacheBean
//     */
//    public void get(
//            final Map<String, String> parms,
//            RseponseHander responseHander,
//            BaseCacheBean cacheBean) {
//        LogUtil.i(true, TAG, "【HttpVolley.post()】【url=" + responseHander.getmUrl() + ",parms="
//                + parms + "】");
//        mResponseHander = responseHander;
//        if (!NetUtil.isConnect(mContext)) {
//            if (mResponseHander.mThirdPrtyListener == null) {
//                loadResult(null, false, "网络连接失败", "-0");
//            } else {
//                HttpMessage httpMessage = new HttpMessage(mResponseHander.mEent, false,
//                        "网络连接失败", null, "-0", mResponseHander.mObj);
//                mResponseHander.mThirdPrtyListener.onErrorResponse(httpMessage);
//            }
//            return;
//        }
//        if (cacheBean == null) {
//            get(parms, responseHander);
//            return;
//        }
//        StringBuilder id = new StringBuilder(mResponseHander.getmUrl());
//        if (mResponseHander.mObj != null && mResponseHander.mObj.length > 0) {
//            id.append(mResponseHander.mObj[0]);
//        }
//        id.append(parms.get(AbKeys.DATA));
//        mCacheBean = cacheBean;
////        mCacheBean.setUpLoadData(parms.get(AbKeys.DATA));
//        mCacheBean.setId(id.toString());
//
//        //缓存与HTTP共存使用
//        if (mCacheBean.isCacheAndHttp()) {
//
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    BaseCacheBean bean = CacheManager.getInstance(mContext).getCacheInfo(mCacheBean.getId());
//                    if (bean != null && (bean.getCacheId() != null)
//                            && bean.getCacheId().equals(mCacheBean.getCacheId())) {
//
//                        if (mResponseHander.mThirdPrtyListener == null) {
//                            handleCacheContent(bean.getResultJson());
//                        } else {
//                            Object obj = null;
//                            if (mResponseHander.mResultAllToken != null) {
//                                obj = JSON.parseObject(bean.getResultJson(), mResponseHander.mResultAllToken.getType());
//                            }
//                            HttpMessage message = new HttpMessage(mResponseHander.mEent, true, "",
//                                    obj, "", mResponseHander.mObj);
//                            mResponseHander.mThirdPrtyListener.onSuccess(message);
//                        }
//
//                    }
//                    getRequest(parms);
//                }
//            }).start();
//
//        } else {//先取缓存,后访问网络
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    BaseCacheBean bean = CacheManager.getInstance(mContext).getCacheInfo(mCacheBean.getId());
//                    if (bean == null) {
//                        getRequest(parms);
//                    } else {
//                        long duration = mCacheBean.getDurationTime();
//                        long puttime = bean.getInputTime();
//                        long time = System.currentTimeMillis();
//                        if ((time - puttime) <= duration && (bean.getCacheId() != null)
//                                && bean.getCacheId().equals(mCacheBean.getCacheId())) {
//                            if (mResponseHander.mThirdPrtyListener == null) {
//                                handleCacheContent(bean.getResultJson());
//                            } else {
//                                Object obj = null;
//                                if (mResponseHander.mResultAllToken != null) {
//                                    obj = JSON.parseObject(bean.getResultJson(), mResponseHander.mResultAllToken.getType());
//                                }
//                                HttpMessage message = new HttpMessage(mResponseHander.mEent, true, "",
//                                        obj, "", mResponseHander.mObj);
//                                mResponseHander.mThirdPrtyListener.onSuccess(message);
//                            }
//                        } else {
//                            getRequest(parms);
//                            /**
//                             * 过期的缓存删除
//                             */
//                            long rrr = CacheManager.getInstance(mContext).removeCacheInfo(mCacheBean.getId());
//                            LogUtil.i(true, TAG, "HttpVolley: post: [000000000]=" + rrr);
//                        }
//                    }
//                }
//            }).start();
//
//        }
//
//    }
//
//    private void postRequest(final Map<String, String> parms) {
//        StringRequest request = new StringRequest(Method.POST, mResponseHander.getmUrl(),
//                new Listener<String>() {
//
//                    @Override
//                    public void onResponse(String response) {
//                        if (mResponseHander.mThirdPrtyListener == null) {
//                            handleResponseContent(response);
//                        } else {
//                            Object obj = null;
//                            if (mResponseHander.mResultAllToken != null) {
//                                obj = JSON.parseObject(response, mResponseHander.mResultAllToken.getType());
//                            }
//                            HttpMessage message = new HttpMessage(mResponseHander.mEent, true, "",
//                                    obj, "", mResponseHander.mObj);
//                            mResponseHander.mThirdPrtyListener.onSuccess(message);
//                            if (mCacheBean != null) {
//                                saveCache(response);
//                            }
//                        }
//                        System.gc();
//                    }
//                }, new ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                String str = error.toString();
//                LogUtil.i(true, TAG, "HttpVolley: onErrorResponse: [11111111]=" + str);
//                String desprition = null;
//                String errorcode;
//                if (str.contains("TimeoutError")) {
//                    desprition = "网络连接超时";
//                    errorcode = ServiceType.TIME_OUT;
//                } else if (str.contains("NetworkError")) {
//                    desprition = "网络连接失败";
//                    errorcode = ServiceType.NET_WORK_ERROR;
//                } else {
//                    desprition = "服务器连接失败";
//                    if (error.networkResponse == null) {
//
//                        errorcode = ServiceType.SERVICE_ERROR;
//                    } else {
//                        errorcode = error.networkResponse.statusCode + "";
//                    }
//                }
//
//                if (mResponseHander.mThirdPrtyListener == null) {
//                    loadResult(null, false, desprition, errorcode);
//                } else {
//                    HttpMessage httpMessage = new HttpMessage(mResponseHander.mEent, false,
//                            desprition, error.networkResponse.statusCode + "", null, mResponseHander.mObj);
//                    mResponseHander.mThirdPrtyListener.onErrorResponse(httpMessage);
//                }
//            }
//
//        }, new LoadingListener() {
//
//            @Override
//            public void onLoading(long count, long current) {
//
//            }
//
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                return parms;
//            }
//
//        };
//        request.setTag(mResponseHander.getmUrl());
//        CustomApplication.mVolleyQueue.add(request);
//    }
//
//    /**
//     * 处理请求返回的内容，将数据转换后回调给数据请求者
//     *
//     * @param respContent
//     */
//    void handleResponseContent(String respContent) {
//        LogUtil.i(true, TAG, "HttpVolley: handleResponseContent: [respContent=]" + respContent);
//        HttpResponse httpResponse = null;
//
//        try {
//            httpResponse = handleHttpResult(respContent, httpResponse);
//            saveCache(respContent);
//        } catch (Exception e) {
//            LogUtil.i(true, TAG, "HttpVolley: handleResponseContent: [222111111111]=" + e);
//            String content = "服务器数据返回错误";
//            if (httpResponse != null) {
//                loadResult(httpResponse, false, content, httpResponse.msg_code);
//            } else {
//                loadResult(httpResponse, false, content, "");
//            }
////            else {
////                loadResult(null, false, "", "");
////            }
//
//
//        }
//
//
//    }
//
//    /**
//     * 保存缓存信息
//     *
//     * @param respContent
//     */
//    private void saveCache(String respContent) {
//        if (mCacheBean != null) {
//            mCacheBean.setResultJson(respContent);
//            mCacheBean.setInputTime(System.currentTimeMillis());
//            CacheManager.getInstance(mContext).putCacheInfo(mCacheBean);
//        }
//    }
//
//    /**
//     * 处理无缓存的请求结果
//     *
//     * @param respContent
//     * @param httpResponse
//     * @return
//     */
//    @NonNull
//    private HttpResponse handleHttpResult(String respContent, HttpResponse httpResponse) {
//        //为空时解析整个json数据
//        if (mResponseHander.mResultDataToken == null) {
//            //不为空则解析整个数据
//            if (mResponseHander.mResultAllToken != null) {
//                httpResponse = JSON.parseObject(respContent,
//                        mResponseHander.mResultAllToken.getType());
//                /**
//                 * 如果失败
//                 */
//                if (isFailed(httpResponse)) {
//                    loadResult(null, false, httpResponse.msg, httpResponse.msg_code);
//                } else {//将解析成功的数据抛出去
//                    loadResult(httpResponse, true, httpResponse.msg, httpResponse.msg_code);
//                }
//            } else {//为空时抛出原始数据
//                loadResult(respContent, true, httpResponse.msg, httpResponse.msg_code);
//            }
//        } else {
//            httpResponse = JSON.parseObject(respContent,
//                    HttpResponse.class);
//            Object dataObject = null;
//            /**
//             * 如果失败
//             */
//            if (isFailed(httpResponse)) {
//                loadResult(null, false, httpResponse.msg, httpResponse.msg_code);
//            } else {
//                String dataStr = JSON.toJSONString(httpResponse.data);
//
//                dataObject = JSON.parseObject(dataStr,
//                        mResponseHander.mResultDataToken.getType());
//                loadResult(dataObject, true, httpResponse.msg, httpResponse.msg_code);
//            }
//        }
//        return httpResponse;
//    }
//
//    /**
//     * 处理请求返回的内容，将数据转换后回调给数据请求者
//     *
//     * @param respContent
//     */
//    void handleCacheContent(String respContent) {
//        LogUtil.i(true, TAG, "HttpVolley: handleCacheContent: [respContent=]" + respContent);
//        HttpResponse httpResponse = null;
//        try {
//            //为空时解析整个json数据
//            httpResponse = handleHttpResult(respContent, httpResponse);
//        } catch (Exception e) {
//            LogUtil.i(true, TAG, "HttpVolley: handleCacheContent: [222111111111]=" + e);
//            String content = "服务器数据返回错误";
//            if (httpResponse != null) {
//                loadResult(httpResponse, false, content, httpResponse.msg_code);
//            } else {
//                loadResult(httpResponse, false, content, "");
//            }
//
//        }
//    }
//
//    /**
//     * @param httpResponse
//     * @param success
//     * @param msg
//     * @param msg_code
//     */
//    private void loadResult(Object httpResponse, boolean success, String msg, String msg_code) {
//        HttpMessage httpMessage1 = new HttpMessage(mResponseHander.mEent,
//                success, msg, httpResponse, msg_code,
//                mResponseHander.mObj);
//        CustomApplication.mVolleyQueue.cancelAll(mResponseHander.getmUrl());
//        if (mResponseHander.mLoadingListener == null) {
//            EventBus.getDefault().post(httpMessage1);
//        } else {
//            if (success) {
//                mResponseHander.mLoadingListener.onSuccess(httpMessage1);
//            } else {
//                mResponseHander.mLoadingListener.onErrorResponse(httpMessage1);
//            }
//        }
//    }
//
//    private boolean isFailed(HttpResponse httpResponse) {
//        return httpResponse == null || StringUtils.isEmpty(httpResponse.msg_code)
//                || !(httpResponse.msg_code.equals("1") ||
//                httpResponse.msg_code.equals("9004"));
//    }
//}
