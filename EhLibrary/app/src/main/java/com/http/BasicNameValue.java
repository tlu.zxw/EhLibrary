package com.http;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright
 * 
 * @author zhangxiaowei
 * 
 */
public class BasicNameValue {
	private List<BasicNameValuePair> params;

	public BasicNameValue() {
		params = new ArrayList<BasicNameValuePair>();
	}

	public void put(String key, String values) {
		BasicNameValuePair pair = new BasicNameValuePair(key, values);
		params.add(pair);
	}

	public List<BasicNameValuePair> getParams() {
		return params;
	}
}
