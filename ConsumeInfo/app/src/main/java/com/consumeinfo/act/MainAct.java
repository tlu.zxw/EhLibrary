package com.consumeinfo.act;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.consumeinfo.R;
import com.consumeinfo.bean.ConsumeInfo;
import com.consumeinfo.util.DialogEh;

import cn.bmob.v3.listener.InsertListener;

/**
 * Created by zhangxiaowei on 17/3/12.
 */

public class MainAct extends Activity {
    EditText money, detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        money = (EditText) findViewById(R.id.money);
        detail = (EditText) findViewById(R.id.detail);


        findViewById(R.id.comit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comit();
            }

            private void comit() {
                DialogEh dialogEh = new DialogEh(MainAct.this);
                dialogEh.setTitle("确定提交？");
                dialogEh.setClickListener(new DialogEh.ClickListener() {
                    @Override
                    public void onCacel(View arg0) {

                    }

                    @Override
                    public void onConfirm(View arg0) {
                        ConsumeInfo info = new ConsumeInfo();
                        if(info.getUserid().contains("155")){
                            info.setUsername("张效伟");
                        }else{
                            info.setUsername("汤传军");
                        }
                        info.setStatus("0");
                        info.setMoney(money.getText().toString());
                        info.setDetail(detail.getText().toString());
                        info.insertObject(MainAct.this, new InsertListener() {
                            @Override
                            public void onSuccess() {
                                toast("成功");
                            }

                            @Override
                            public void onFailure(String s) {
                                toast(s);
                            }
                        });
                    }

                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });
                dialogEh.show();

            }
        });
        findViewById(R.id.detailact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inttant = new Intent(MainAct.this, DetailAct.class);
                startActivity(inttant);
            }
        });
    }

    public void toast(final String str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainAct.this, str, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }
}
