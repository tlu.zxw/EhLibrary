package com.consumeinfo.act;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.consumeinfo.R;
import com.consumeinfo.adapter.DetailAdapter;
import com.consumeinfo.bean.ConsumeInfo;
import com.consumeinfo.util.Arithmetic;
import com.consumeinfo.util.DialogEh;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * Created by zhangxiaowei on 17/3/12.
 */

public class DetailAct extends Activity {
    DetailAdapter mDetailAdapter;
    RecyclerView mRecyclerView;
    String sum = "0", zhang = "0", tang = "0";
    TextView tvsum, tvzhang, tvtang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_detail);
        mDetailAdapter = new DetailAdapter(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mDetailAdapter);
        tvsum = (TextView) findViewById(R.id.allmoey);
        tvzhang = (TextView) findViewById(R.id.zhang);
        tvtang = (TextView) findViewById(R.id.tang);
        BmobQuery<ConsumeInfo> query = new BmobQuery<ConsumeInfo>();
        query.addWhereEqualTo("status", "0");
        query.setLimit(300);
        query.order("time");
        query.findObjects(this, new FindListener<ConsumeInfo>() {
            @Override
            public void onSuccess(List<ConsumeInfo> list) {
                mDetailAdapter.notifyData(list);
                for (ConsumeInfo info : list) {
                    sum = Arithmetic.jia(sum, info.getMoney());
                    if ("15555986853".equals(info.getUserid())) {
                        zhang = Arithmetic.jia(zhang, info.getMoney());
                    } else {
                        tang = Arithmetic.jia(tang, info.getMoney());
                    }
                }

                initmoney();
            }

            @Override
            public void onError(String s) {
                toast(s);
            }
        });
        findViewById(R.id.jiesuan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogEh dialogEh = new DialogEh(DetailAct.this);
                dialogEh.setClickListener(new DialogEh.ClickListener() {
                    @Override
                    public void onCacel(View arg0) {

                    }

                    @Override
                    public void onConfirm(View arg0) {
                        List<BmobObject> obj = new ArrayList<BmobObject>();
                        List<ConsumeInfo> info = mDetailAdapter.getData();
                        for (ConsumeInfo info1 : info) {
                            info1.setStatus("1");
                            obj.add(info1);
                        }
                        new BmobObject().updateBatch(DetailAct.this, obj, new UpdateListener() {
                            @Override
                            public void onSuccess() {
                                toast("成功");
                                mDetailAdapter.getData().removeAll(mDetailAdapter.getData());
                                mDetailAdapter.notifyData(mDetailAdapter.getData());
                                sum = "0";
                                zhang = "0";
                                tang = "0";
                                initmoney();
                            }

                            @Override
                            public void onFailure(String s) {
                                toast(s);
                            }
                        });
                    }

                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });
                dialogEh.show();
            }
        });
    }

    private void initmoney() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String string = Arithmetic.chu(sum, "2");
                tvsum.setText(sum);
                tvzhang.setText(zhang + " : " + Arithmetic.jian(zhang, string));
                tvtang.setText(tang + " : " + Arithmetic.jian(tang, string));
            }
        });
    }

    public void toast(final String str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DetailAct.this, str, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }
}
