package com.consumeinfo.bean;

import cn.bmob.v3.BmobObject;

/**
 * Created by zhangxiaowei on 17/3/12.
 */

public class ConsumeInfo extends BmobObject {
//    String userid="15555986853";
    String userid="17602150250";
    String detail;
    String money;
    String username;
    boolean isCheckOut;
    String time;
    /**
     * 0是未结算，1是已结算
     */
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public ConsumeInfo() {
        time=System.currentTimeMillis()+"";
    }

    @Override
    public String toString() {
        return "ConsumeInfo{" +
                "userid='" + userid + '\'' +
                ", detail='" + detail + '\'' +
                ", money='" + money + '\'' +
                ", username='" + username + '\'' +
                ", isCheckOut=" + isCheckOut +
                ", status='" + status + '\'' +
                '}';
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserid() {
        return userid;
    }

    public String getDetail() {
        return detail;
    }

    public String getMoney() {
        return money;
    }

    public String getUsername() {
        return username;
    }

    public boolean isCheckOut() {
        return isCheckOut;
    }

    public void setCheckOut(boolean checkOut) {
        isCheckOut = checkOut;
    }
}
