package com.consumeinfo.util;

import java.math.BigDecimal;

/**
 * Created by zhangxiaowei on 16/1/5.
 */
public class Arithmetic {

    public static String jia(String f, String i) {
        BigDecimal b1 = new BigDecimal(f);
        BigDecimal b2 = new BigDecimal(i);
        return b1.add(b2)
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }

    public static String cheng(String f, String i) {
        BigDecimal b1 = new BigDecimal(f);
        BigDecimal b2 = new BigDecimal(i);
        return b1.multiply(b2)
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }
    public static String chu(String f, String i) {
        BigDecimal b1 = new BigDecimal(f);
        BigDecimal b2 = new BigDecimal(i);
        return b1.divide(b2)
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }

    public static String jian(String f, String i) {
        BigDecimal b1 = new BigDecimal(f);
        BigDecimal b2 = new BigDecimal(i);
        return b1.subtract(b2)
                .setScale(2, BigDecimal.ROUND_HALF_UP).toString();

    }

    public static float toFloatMoney(String num) {
        BigDecimal big = new BigDecimal(num);
        BigDecimal result = big.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
        return result.floatValue();
    }

    public static String toMoney(String str) {
        float v = toFloatMoney(str);
        if (v <= 0) {
            return "0.01";
        }
        BigDecimal big = new BigDecimal(str);
        BigDecimal result = big.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
        return result.toString();
    }

    public static String toCommonMoney(String str) {
        BigDecimal big = new BigDecimal(str);
        BigDecimal result = big.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
        return result.toString();
    }

}
