package com.consumeinfo.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.consumeinfo.R;

public class DialogEh {
    Context mContext;
    ClickSingleCancelListener mClickSingleCancelListener;
    ClickListener mClickListener;
    Dialog mDialog;
    private TextView mTvContent, mTvSingleCancel;
    RelativeLayout mLayout;
    RelativeLayout mRelativeLayout;
    private String mContent;
    private boolean mSingleCancel;
    private int mContentid;
    /**
     * 是否显示按钮
     */
    private boolean mVisible = true;
    private TextView mConfirm;
    private TextView mCancel, mTitle;

    public DialogEh(Context context) {
        this.mContext = context;
        createDialog();
    }

    /**
     * 取消，确定按钮事件
     */
    public interface ClickListener {
        void onCacel(View arg0);

        void onConfirm(View arg0);

        void onDismiss(DialogInterface dialog);

    }

    public boolean isShow() {

        return mDialog != null && mDialog.isShowing();
    }

    /**
     * 只有一个按钮键
     */
    public interface ClickSingleCancelListener {
        void onCancel(View arg0);

        void onDismiss(DialogInterface dialog);
    }

    public void setCancelable(boolean boo) {
        mDialog.setCancelable(boo);
    }

    public void setOutCancelable(boolean boo) {
        mDialog.setCanceledOnTouchOutside(boo);
    }

    public void setCanCancleable(boolean boo) {
        mDialog.setCancelable(boo);
    }

    private void createDialog() {
        mDialog = new Dialog(mContext, R.style.Dialog_No_Border);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // mDialog.setCancelable(false);
        LayoutInflater m_inflater = LayoutInflater.from(mContext);
        View layout = m_inflater.inflate(R.layout.dialog_eh, null);
        mTvContent = (TextView) layout.findViewById(R.id.dialog_eh_content);
        mTitle = (TextView) layout.findViewById(R.id.dialog_eh_title);

        mLayout = (RelativeLayout) layout.findViewById(R.id.dialog_eh_layout);
        mTvSingleCancel = (TextView) layout
                .findViewById(R.id.dialog_eh_singlecancel);
        mRelativeLayout = (RelativeLayout) layout
                .findViewById(R.id.dialog_eh_parentlayout);
        clickCancel(layout);
        clickOk(layout);
        clickCancelSignle();
        mDialog.setContentView(layout);
        mDialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mClickListener != null) {
                    mClickListener.onDismiss(dialog);
                }
                if (mClickSingleCancelListener != null) {
                    mClickSingleCancelListener.onDismiss(dialog);
                }
            }
        });

    }

    /**
     * 只有一个取消按钮
     *
     * @param listener
     */
    public void setSingleClickListener(ClickSingleCancelListener listener) {
        mSingleCancel = true;
        mClickSingleCancelListener = listener;
        mLayout.setVisibility(View.GONE);
        mTvSingleCancel.setVisibility(View.VISIBLE);
    }

    private void clickCancelSignle() {
        mTvSingleCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickSingleCancelListener != null) {
                    mClickSingleCancelListener.onCancel(v);
                    mDialog.dismiss();
                }
            }
        });
    }

    private void clickOk(View layout) {
        mConfirm = (TextView) layout.findViewById(R.id.dialog_eh_ok);
        mConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (mClickListener != null) {
                    mClickListener.onConfirm(arg0);
                }
                mDialog.dismiss();
                System.gc();
            }
        });
    }

    public void setTitle(int id) {
        mTitle.setText(id);
        mTitle.setVisibility(View.VISIBLE);
    }

    public void setTitleColor(int id) {
        mTitle.setTextColor(mContext.getResources().getColor(id));
    }

    public void setTitle(String str) {
        mTitle.setText(str);
        mTitle.setVisibility(View.VISIBLE);
    }

    public void setConfirmText(int id) {
        mConfirm.setText(id);
        mLayout.setVisibility(View.VISIBLE);
        mTvSingleCancel.setVisibility(View.GONE);
    }

    public void setConfirmText(String str) {
        mConfirm.setText(str);
        mLayout.setVisibility(View.VISIBLE);
        mTvSingleCancel.setVisibility(View.GONE);
    }

    public void setSingleText(int id) {
        mTvSingleCancel.setText(id);
        mLayout.setVisibility(View.GONE);
        mTvSingleCancel.setVisibility(View.VISIBLE);
    }

    public void setSingleText(String str) {
        mTvSingleCancel.setText(str);
        mLayout.setVisibility(View.GONE);
        mTvSingleCancel.setVisibility(View.VISIBLE);
    }

    public void setCancelText(int id) {
        mCancel.setText(id);
        mLayout.setVisibility(View.VISIBLE);
        mTvSingleCancel.setVisibility(View.GONE);
    }

    public void setCancelText(String str) {
        mCancel.setText(str);
        mLayout.setVisibility(View.VISIBLE);
        mLayout.setVisibility(View.VISIBLE);
        mTvSingleCancel.setVisibility(View.GONE);
    }

    private void clickCancel(View layout) {
        mCancel = (TextView) layout.findViewById(R.id.dialog_eh_cancel);
        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mClickListener != null) {
                    mClickListener.onCacel(arg0);
                }
                mDialog.dismiss();
                System.gc();
            }
        });
    }

    Duration mDuration;

    public void setDuration(final Duration duration) {
        mDuration = duration;

    }

    public interface Duration {

        void onFinish();
    }

    /**
     * 设置点击事件
     *
     * @param listener
     */
    public void setClickListener(ClickListener listener) {
        mClickListener = listener;
        mLayout.setVisibility(View.VISIBLE);
        mTvSingleCancel.setVisibility(View.GONE);
    }

    /**
     * 设置底部按钮是否显示
     *
     * @param bol
     */
    public void setButtomVisible(boolean bol) {
        mVisible = bol;
        if (mVisible) {
            mRelativeLayout.setVisibility(View.VISIBLE);
        } else {
            mRelativeLayout.setVisibility(View.GONE);
        }
    }

    /**
     * 设置内容
     *
     * @param content
     */
    public void setContent(String content) {
        mContent = content;
        mTvContent.setText(content);
        mTvContent.setVisibility(View.VISIBLE);
    }

    public void setContent(int id) {
        mContentid = id;
        mTvContent.setText(id);
        mTvContent.setVisibility(View.VISIBLE);
    }

    /**
     * @param content
     * @param maxlines
     */
    public void setContent(String content, int maxlines) {
        mTvContent.setMaxLines(maxlines);
        mTvContent.setEllipsize(TextUtils.TruncateAt.END);
        setContent(content);
    }

    /**
     * @param id
     * @param maxlines
     */
    public void setContent(int id, int maxlines) {
        mTvContent.setMaxLines(maxlines);
        mTvContent.setEllipsize(TextUtils.TruncateAt.END);
        setContent(id);
    }

    public void setContentGravity(int Gravity) {
        mTvContent.setGravity(Gravity);
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public void setOnDismissListener(OnDismissListener listener) {
        mDialog.setOnDismissListener(listener);
    }

    /**
     * 显示对话框
     */
    public void show() {
        mDialog.show();
        if (mDuration != null) {
            ValueAnimator ani = ValueAnimator.ofInt(10);
            ani.setDuration(2000);
            ani.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    // TODO Auto-generated method stub
                    super.onAnimationEnd(animation);
                    if (mDuration != null) {
                        mDuration.onFinish();
                        mDialog.dismiss();
                    }

                }

            });

            ani.start();
        }
    }
}
