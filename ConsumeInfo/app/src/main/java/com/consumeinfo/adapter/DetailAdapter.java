package com.consumeinfo.adapter;

import android.content.Context;
import android.widget.TextView;

import com.consumeinfo.R;
import com.consumeinfo.bean.ConsumeInfo;

/**
 * Created by zhangxiaowei on 17/3/12.
 */

public class DetailAdapter extends BaseRecyclerAdp<ConsumeInfo,DetailAdapter.ViewHolder> {
    public DetailAdapter(Context context) {
        super(context);
    }

    @Override
    public void onCreateItemView() {
        addItemView(R.layout.item_transaction_query);
    }

    @Override
    public void onBindViewHolder(DetailAdapter.ViewHolder holder, ConsumeInfo data, int position, int viewType) {
        holder.name.setText(data.getUsername());
        holder.money.setText(data.getMoney()+"元");
        holder.time.setText(data.getDetail());
        holder.status.setText(data.getUpdatedAt());
    }

    @Override
    public DetailAdapter.ViewHolder getChildHolder(BaseAdapterEh holder, int viewType) {
        return new ViewHolder(holder,viewType);
    }

    public class ViewHolder extends BaseRecyclerViewHolder {
        public ViewHolder(BaseAdapterEh<ViewHolder> holder, int viewType) {
            super(holder, viewType);
            name = holder.findView(R.id.itq_name);
            money = holder.findView(R.id.itq_money);
            time = holder.findView(R.id.itq_time);
            status = holder.findView(R.id.itq_status);

        }
        TextView name, money, time, status;

    }
}
